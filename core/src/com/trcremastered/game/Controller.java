package com.trcremastered.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.trcremastered.game.model.Dungeon;
import com.trcremastered.game.model.DungeonGenerator;
import com.trcremastered.game.model.RoomMatrix;
import com.trcremastered.game.model.entities.Player;
import com.trcremastered.game.model.inventory.Inventory;
import com.trcremastered.game.model.map.MapGenerator;
import com.trcremastered.game.view.screens.GameScreen;
import com.trcremastered.game.view.screens.MenuScreen;
import com.trcremastered.game.view.screens.Screen;

public class Controller extends ApplicationAdapter implements InputProcessor {

	Timer global;
	Dungeon dungeon;
	Screen screen;
	SpriteBatch batch;

	TiledMapRenderer tmr;
	TiledMap tm;

	RoomMatrix mat;
	ShapeRenderer renderer;

	Viewport viewport;
	OrthographicCamera camera;

	int mouseX;
	int mouseY;
	
	@Override
	public void create () {
		mouseX = 0;
		mouseY = 0;

		screen = new MenuScreen();
		camera = new OrthographicCamera();
		viewport = new FitViewport(screen.getWidth(), screen.getHeight(), camera);

		viewport.apply();
		camera.position.set(screen.getWidth()/2, screen.getHeight()/2, 0);
		camera.update();


		batch = new SpriteBatch();
		renderer = new ShapeRenderer();

		// GLOBAL TIMER
		global = new Timer();
		global.scheduleTask(new Timer.Task() {
			@Override
			public void run() {
				if(dungeon != null){
					dungeon.getPlayer().timer(mat);
				}
			}
		},0,0.004f);

		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(128, 0, 128, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		renderer.setProjectionMatrix(camera.combined);
		camera.update();

		// BACKGROUND OF VIEWPORT
		renderer.begin(ShapeRenderer.ShapeType.Filled);
		renderer.setColor(Color.BLACK);
		renderer.rect(0,0,screen.getWidth(),screen.getHeight());
		renderer.end();

		if(screen.getId() == 1) {
			for (int x = 0; x < dungeon.getLevel()[0].getRooms().length; x++) {
				for (int y = 0; y < dungeon.getLevel()[0].getRooms().length; y++) {
					if (dungeon.getLevel()[0].getRooms()[x][y] != null) {
						tm = dungeon.getLevel()[0].getRooms()[x][y].getMapContainer().getMap();
					}
				}
			}
			tmr = new OrthogonalTiledMapRenderer(tm);
			tmr.setView(camera);
			tmr.render();
		}

		//TEST MATRIX//
		if(mat!= null){
			mat.draw(renderer);
		}
		///////////////

		// PLAYER
		batch.begin();
		screen.render(batch);
		if(dungeon != null) dungeon.getPlayer().render(batch);
		batch.end();

		// MOUSE POSITION
		renderer.begin(ShapeRenderer.ShapeType.Filled);
		renderer.setColor(Color.WHITE);
		renderer.circle(mouseX,mouseY,4);
		renderer.end();
	}
	
	@Override
	public void dispose () {
		global.stop();
		global.clear();
		dungeon.getPlayer().dispose();
		screen.dispose();
		renderer.dispose();
		batch.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		if(dungeon != null){
			dungeon.getPlayer().keyDown(keycode);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(dungeon != null){
			dungeon.getPlayer().keyUp(keycode);
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		//screenY = Gdx.graphics.getHeight()-screenY;
		//screenX = (int) (((float)screenX / (float)Gdx.graphics.getWidth())*screen.getWidth());
		//screenY = (int) (((float)screenY / (float)Gdx.graphics.getHeight())*screen.getHeight());
		Vector2 lol = viewport.unproject(new Vector2(screenX, screenY));
		screenX = (int) lol.x;
		screenY = (int) lol.y;

		button(screen.touchDown(screenX,screenY));
		/**
		 * - TODO -
		 *
		 * calculate relative position out of absolute position
		 *
 		 */

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		//screenY = Gdx.graphics.getHeight()-screenY;
		//screenX = (int) (((float)screenX / (float)Gdx.graphics.getWidth())*screen.getWidth());
		//screenY = (int) (((float)screenY / (float)Gdx.graphics.getHeight())*screen.getHeight());
		Vector2 lol = viewport.unproject(new Vector2(screenX, screenY));
		mouseX = (int) lol.x;
		mouseY = (int) lol.y;
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public void button(int id){
		switch(id){
			case 0:
				//TEST MATRIX//
				// FIRST INT WIDTH
				// SECONT INT HEIGHT
				int matrix[][] = new int[11][7];
				for(int i = 0; i < matrix.length; i++){
					for(int n = 0; n < matrix[0].length; n++){
						if(i == matrix.length-1 || i == 0 || n == matrix[0].length-1 || n == 0){
							if(i != 5 && n != 3)
								matrix[i][n] = 1;
						}
					}
				}
				matrix[6][5] = 1;
				matrix[4][4] = 1;
				matrix[2][2] = 1;
				mat = new RoomMatrix(matrix);
				/////////////////

				Texture[] tiles = new Texture[7];
				for(int i = 1; i < 8; i++){
					tiles[i-1] = new Texture("tilesets/tileset_floor_" + i + ".png");
				}

				dungeon = new DungeonGenerator().generateDungeon(5, 9, 48, (new Player(1,50,50,new Inventory(null),"sprites/player/player_0_m.png")));
				new MapGenerator(tiles).generateMap(dungeon);
				screen = new GameScreen();

				camera = new OrthographicCamera();
				viewport = new FitViewport(screen.getWidth(), screen.getHeight(), camera);
				viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				camera.position.set(screen.getWidth()/2-237, screen.getHeight()/2, 0);
				camera.update();
				viewport.apply();
		}
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}
}
