package com.trcremastered.game.model.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.trcremastered.game.model.RoomMatrix;
import com.trcremastered.game.model.inventory.Inventory;

public abstract class Entity {

    Inventory inv;

    int lvl;
    int exp;
    int dmg;
    int def;
    float speed; // float? erlaubt mehr variabilität und nicht nur 3 zu langsam 4 zu schnell
    int hp;
    int maxHp;

    float movementX;
    float movementY;

    int animationType;
    /**
     * ENTITIES:
     * 0: walk
     * 1: idle
     * 2: attack
     * 3: moment of attack    goes to state 3, exactly in the moment the attack hits? then back to 2
     *
     * PLAYER:
     * 0: walk
     * 1: attack (laser)
     * 2: idle
     * 3: attack (sword)
     */
    int animationState;

    Sprite sprite;
    TextureRegion[][] textureRegion;

    int timerCounter;

    boolean flipped;

    public Entity(int lvl, Inventory inv) {
        this.lvl = lvl; // Stats abhängig von lvl
        // exp?
        this.inv = inv; // Items übergeben?
        animationState = 0;
    }



    abstract public void timer(RoomMatrix matrix);      // aufgerufen durch global Timer
    abstract public void calcStats();                   // berechnet stats anhand von Level und Items
    abstract public void animation();                   // geht die animation durch
    abstract public void render(SpriteBatch batch);     // wird durch globale render aufgerufen
    abstract public void dispose();                     // disposed Sprite, bzw. die texture

    public void move(RoomMatrix matrix){    // MOVE METHOD WITH COLLISIONS THE SAME FOR EVERY ENTITY?
        if(matrix.possibleMove(sprite.getX(), sprite.getY(), movementX, movementY)){
            sprite.setX(sprite.getX()+movementX);
            sprite.setY(sprite.getY()+movementY);
        }
        else if(matrix.possibleXMove(sprite.getX(),sprite.getY(),movementX)){
            sprite.setX(sprite.getX()+movementX);
        }
        else if(matrix.possibleYMove(sprite.getX(),sprite.getY(),movementY)){
            sprite.setY(sprite.getY()+movementY);
        }
    }


    // GETTER SETTER

    public int getxPos() {
        return (int)sprite.getX();
    }

    public void setxPos(int xPos) {
        this.sprite.setX(xPos);
    }

    public int getyPos() {
        return (int)sprite.getY();
    }

    public void setyPos(int yPos) {
        this.sprite.setY(yPos);
    }

    public Inventory getInv() {
        return inv;
    }

    public void setInv(Inventory inv) {
        this.inv = inv;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public float getMovementX() {
        return movementX;
    }

    public void setMovementX(float movementX) {
        this.movementX = movementX;
    }

    public float getMovementY() {
        return movementY;
    }

    public void setMovementY(float movementY) {
        this.movementY = movementY;
    }

    public int getAnimationState() {
        return animationState;
    }

    public void setAnimationState(int animationState) {
        this.animationState = animationState;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public TextureRegion[][] getTextureRegion() {
        return textureRegion;
    }

    public void setTextureRegion(TextureRegion[][] textureRegion) {
        this.textureRegion = textureRegion;
    }

    public int getAnimationType() {
        return animationType;
    }

    public void setAnimationType(int animationType) {
        this.animationType = animationType;
    }
}
