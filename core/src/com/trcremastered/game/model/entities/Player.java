package com.trcremastered.game.model.entities;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.trcremastered.game.model.RoomMatrix;
import com.trcremastered.game.model.inventory.Inventory;

public class Player extends Entity{
    public Player(int lvl, int xPos, int yPos, Inventory inv, String texture) {
        super(lvl, inv);

        textureRegion = TextureRegion.split(new Texture(texture),64,64);
        this.sprite = new Sprite(textureRegion[animationType][animationState]);
        this.sprite.setX(xPos);
        this.sprite.setY(yPos);
        speed = 1.8f;
    }

    @Override
    public void timer(RoomMatrix matrix) {
        timerCounter++;
        if(timerCounter % 25 == 0){
            updateAnimation();
        }
        if(timerCounter % 2 == 0){
            move(matrix);
        }
        if(timerCounter > 1000){
            timerCounter = 0;
        }
    }

    @Override
    public void calcStats() {

    }

    @Override
    public void animation() {

    }

    @Override
    public void render(SpriteBatch batch) {
        sprite.setFlip(flipped,false);
        sprite.draw(batch);
    }

    @Override
    public void dispose() {
        sprite.getTexture().dispose();
    }

    public void updateAnimation(){
        if(movementX == 0 && movementY == 0 && animationType == 0) animationType = 2;
        else if(animationType == 2 && (movementX != 0 || movementY != 0)) animationType = 0;
        animationState++;
        if(animationState > 9 && animationType != 1){
            animationState = 0;
            if(animationType == 3){
                animationType = 0;
                movementX = movementX * 2f;
                movementY = movementY * 2f;
                speed = speed*2;
            }
        }
        else if(animationType == 1 && animationState > 3){
            animationState = 0;
            animationType = 0;
        }
        this.sprite.setRegion(textureRegion[animationType][animationState]);
        if(animationType != 3)  // REMOVE TO ALLOW PLAER ROTATING WHILE ATTACKING
        updateFlip();
    }

    public void updateFlip(){
        if(movementX < 0){
            flipped = true;
        }
        else if(movementX > 0){
            flipped = false;
        }
    }


    public void keyDown(int keycode){
        switch(keycode){
            case Input.Keys.W:
                movementY =speed;
                break;
            case Input.Keys.A:
                movementX = -speed;
                break;
            case Input.Keys.S:
                movementY = -speed;
                break;
            case Input.Keys.D:
                movementX = speed;
                break;
            case Input.Keys.SPACE:
                if(animationType != 3){
                    animationType = 3;
                    movementX = movementX * 0.5f;
                    movementY = movementY * 0.5f;
                    speed = speed * 0.5f;
                    animationState = 0;
                }
                break;
            case Input.Keys.UP:
                if(animationType != 1 && animationType != 3){
                    animationType = 1;
                    animationState = 0;
                }
                break;
            case Input.Keys.RIGHT:
                if(animationType != 1 && animationType != 3){
                    animationType = 1;
                    animationState = 0;
                }
                break;
            case Input.Keys.DOWN:
                if(animationType != 1 && animationType != 3){
                    animationType = 1;
                    animationState = 0;
                }
                break;
            case Input.Keys.LEFT:
                if(animationType != 1 && animationType != 3){
                    animationType = 1;
                    animationState = 0;
                }
                break;
        }
    }

    public void keyUp(int keycode){
        switch(keycode){
            case Input.Keys.W:
                if(movementY > 0) movementY = 0;
                break;
            case Input.Keys.A:
                if(movementX < 0) movementX = 0;
                break;
            case Input.Keys.S:
                if(movementY < 0) movementY = 0;
                break;
            case Input.Keys.D:
                if(movementX > 0) movementX = 0;
                break;
        }
    }
}
