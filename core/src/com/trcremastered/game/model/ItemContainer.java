package com.trcremastered.game.model;

import com.trcremastered.game.model.inventory.items.Item;

public class ItemContainer {
    private int xPos;
    private int yPos;
    private Item item;

    public ItemContainer(int xPos, int yPos, Item item){
        this.xPos = xPos;
        this.yPos = yPos;
        this.item = item;
    }

    /**
     * @return the xPos
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * @param xPos the xPos to set
     *
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * @return the yPos
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * @param yPos the yPos to set
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

}
