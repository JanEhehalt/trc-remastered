package com.trcremastered.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;

public class RoomMatrix {
    int[][] matrix;
    ArrayList<Rectangle> rects;
    /**
     * 0: entities can walk
     * 1: entities can't walk
     * TODO - 2: entities can walk with key -- key will be removed from their inventory
     */

    public RoomMatrix(int[][] matrix){
        this.matrix = matrix;
        rects = new ArrayList();
        for(int i = 0; i < matrix.length; i++){
            for(int n = 0; n < matrix[0].length; n++){
                if(matrix[i][n] == 1){
                    rects.add(new Rectangle(i*48,n*48,48,48));
                }
            }
        }

    }

    public boolean possibleMove(float xPos, float yPos, float movementX, float movementY){
        boolean isPossible = true;
        int entityWidth = 32;
        int entityHeight = 32;
        Rectangle collisionRect = new Rectangle(xPos+16+movementX,yPos+movementY,entityWidth,entityHeight);
        for(Rectangle rect : rects){
            //if(Math.sqrt(Math.pow((rect.x - collisionRect.x), 2) + Math.pow((rect.y - collisionRect.y), 2)) < 5){
                if(Intersector.overlaps(collisionRect, rect)){
                    isPossible = false;
                    break;
                }
            //}
        }
        return isPossible;
    }

    public boolean possibleXMove(float xPos, float yPos, float movementX){
        boolean isPossible = true;
        int entityWidth = 32;
        int entityHeight = 32;
        Rectangle collisionRect = new Rectangle(xPos+16+movementX,yPos,entityWidth,entityHeight);
        for(Rectangle rect : rects){
            //if(Math.sqrt(Math.pow((rect.x - collisionRect.x), 2) + Math.pow((rect.y - collisionRect.y), 2)) < 92){
                if(Intersector.overlaps(collisionRect, rect)){
                    isPossible = false;
                    break;
                }
            //}
        }
        return isPossible;
    }

    public boolean possibleYMove(float xPos, float yPos,  float movementY){
        boolean isPossible = true;
        int entityWidth = 32;
        int entityHeight = 32;
        Rectangle collisionRect = new Rectangle(xPos+16,yPos+movementY,entityWidth,entityHeight);
        for(Rectangle rect : rects){
            //if(Math.sqrt(Math.pow((rect.x - collisionRect.x), 2) + Math.pow((rect.y - collisionRect.y), 2)) < 92){
                if(Intersector.overlaps(collisionRect, rect)){
                    isPossible = false;
                    break;
                }
            //}
        }
        return isPossible;
    }

    public void draw(ShapeRenderer renderer){
        renderer.setColor(Color.RED);
        if(renderer.isDrawing()) renderer.end();
        renderer.begin(ShapeRenderer.ShapeType.Line);
        for(Rectangle rect : rects){
            renderer.rect(rect.getX(),rect.getY(),rect.getWidth(),rect.getHeight());
        }
        renderer.end();
    }
}
