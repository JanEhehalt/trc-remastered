package com.trcremastered.game.model;

import com.trcremastered.game.model.entities.Entity;
import com.trcremastered.game.model.inventory.items.Item;
import com.trcremastered.game.model.map.MapContainer;

import java.util.ArrayList;

public class Room {
    private ArrayList<ItemContainer> items;
    private Entity[] enemies;
    private MapContainer mapContainer;
    int lvl;

    public Room(ArrayList<ItemContainer> items, Entity[] enemies){
        this.items = items;
        this.enemies = enemies;
    }

    public void spawnEnemies(int xPos, int yPos, Entity enemy){
        enemy.setxPos(xPos);
        enemy.setyPos(yPos);
    }

    public void spawnItem(int xPos, int yPos, Item i){
        items.add(new ItemContainer(xPos, yPos, i));
    }

    /**
     * @return the enemies
     */
    public Entity[] getEnemies() {
        return enemies;
    }

    /**
     * @param enemies the enemies to set
     */
    public void setEnemies(Entity[] enemies) {
        this.enemies = enemies;
    }

    public void setEnemies(Entity enemy, int i){
        this.enemies[i] = enemy;
    }

    /**
     * @return the items
     */
    public ArrayList<ItemContainer> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(ArrayList<ItemContainer> items) {
        this.items = items;
    }

    public MapContainer getMapContainer() {
        return mapContainer;
    }

    public void setMapContainer(MapContainer mapContainer) {
        this.mapContainer = mapContainer;
    }

}
