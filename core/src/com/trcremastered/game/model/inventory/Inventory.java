package com.trcremastered.game.model.inventory;

import com.trcremastered.game.model.inventory.items.Item;

public class Inventory {

    Item[] items;
    Item[] equipped;

    int selected;

    public Inventory(Item[] items){
        equipped = new Item[2];
        this.items = new Item[6];
        if(items != null){
            for(Item item : items){
                addItem(item);
            }
        }
    }

    public boolean addItem(Item item){
        boolean successful = false;
        for(int i = 0; i < items.length; i++){
            if(items[i] == null){
                items[i] = item;
                successful = true;
                break;
            }
        }
        return successful;
    }

    public void equipItem(int to){
        if(selected > 2){
            Item temp = items[selected-2];
            items[selected-2] = equipped[to];
            equipped[to] = temp;
        }
    }

    public void deleteSelected(){
        if(selected > 2){
            items[selected-2] = null;
        }
        else{
            equipped[selected] = null;
        }
    }

    public Item[] getItems() {
        return items;
    }

    public Item getItem(int place){
        if(place > 2){
            return items[place-2];
        }
        else{
            return equipped[place];
        }
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public Item[] getEquipped() {
        return equipped;
    }

    public void setEquipped(Item[] equipped) {
        this.equipped = equipped;
    }
}
