package com.trcremastered.game.model.inventory.items;

public abstract class Item {

    // stats for !consumable
    int dmgUp;
    int defUp;
    int hpUp;
    float speedUp; // float? - mehr variabilität

    // stats for consumable
    int heal;

    private final int level;
    private final int id;

    float consumable;

    public Item(int level, int id){
        this.level = level;
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
