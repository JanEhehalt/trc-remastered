package com.trcremastered.game.model;

public class Level {
    private Room[][] rooms;
    private int[] exitRoom;

    public Level(int amount){
        this.rooms = new Room[amount][amount];
        exitRoom = new int[2];
    }

    /**
     * @return the rooms
     */
    public Room[][] getRooms() {
        return rooms;
    }

    /**
     * @param rooms the rooms to set
     */
    public void setRooms(Room[][] rooms) {
        this.rooms = rooms;
    }

    public void setRoom(Room room, int x, int y){
        if(x < this.rooms.length && y < this.rooms.length){
            this.rooms[x][y] = room;
        }
    }

    public int[] getExitRoom(){
        return exitRoom;
    }

    public void setExitRoom(int number, int i){
        exitRoom[i] = number;
    }

}
