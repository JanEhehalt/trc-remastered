package com.trcremastered.game.model;

import com.trcremastered.game.model.entities.Player;

public class Dungeon {

    private Level[] level;
    private Player player;

    // 0: room hasn't been found, 1: found, 2: visited.
    // This array is used for drawing the map
    private int[][] drawStateMap;

    public Dungeon(Player player){
        this.level = new Level[7];
        this.player = player;
    }

    public void resetMap(int amount){
        drawStateMap = new int[amount][amount];
    }

    /*
    public void updateMap(int roomX, int roomY){
        // currentRoom is visited, therefore set to 2
        drawStateMap[roomX][roomY] = 2;

        // all neighboring rooms are now found, set to 1, if possible

        // left
        if(roomX > 0 && currentLevel.getRooms()[roomX - 1][roomY] != null){
            if(drawStateMap[roomX - 1][roomY] != 2) {
                drawStateMap[roomX - 1][roomY] = 1;
            }
        }

        // right
        if(roomX < currentLevel.getRooms().length - 1 && currentLevel.getRooms()[roomX + 1][roomY] != null){
            if(drawStateMap[roomX + 1][roomY] != 2) {
                drawStateMap[roomX + 1][roomY] = 1;
            }
        }

        // top
        if(roomY > 0 && currentLevel.getRooms()[roomX][roomY - 1] != null){
            if(drawStateMap[roomX][roomY - 1] != 2) {
                drawStateMap[roomX][roomY - 1] = 1;
            }
        }

        // bottom
        if(roomY < currentLevel.getRooms().length - 1 && currentLevel.getRooms()[roomX][roomY + 1] != null){
            if(drawStateMap[roomX][roomY + 1] != 2) {
                drawStateMap[roomX][roomY + 1] = 1;
            }
        }
    }
    */

    public Level[] getLevel() {
        return level;
    }

    public void setLevel(Level[] level) {
        this.level = level;
    }

    public void setLevel(Level level, int i){
        this.level[i] = level;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int[][] getDrawStateMap() {
        return drawStateMap;
    }

    public void setDrawStateMap(int[][] drawStateMap) {
        this.drawStateMap = drawStateMap;
    }
}
