package com.trcremastered.game.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.trcremastered.game.view.Button;

public class MenuScreen extends Screen {

    public MenuScreen(){
        super(1600,900);
        id = 0;
        buttons.add(new Button(0, 100,600,"sprites/buttons/startButton.png"));
        buttons.add(new Button(1, 100,500,"sprites/buttons/settingsButton.png"));
        buttons.add(new Button(-1, 100,403,"sprites/skinContainer.png"));
        buttons.add(new Button(2, 113,425,"sprites/buttons/left.png"));
        buttons.add(new Button(3, 202,425,"sprites/buttons/right.png"));
        buttons.add(new Button(4, 100,300,"sprites/buttons/quitButton.png"));
    }

    @Override
    public void timer() {

    }

    @Override
    public void render(SpriteBatch batch) {
        for(Button button : buttons){
            button.getSprite().draw(batch);
        }
    }

    @Override
    public int touchDown(int x, int y) {
        System.out.println(x + "   " + y);
        int returnValue = -1;
        Rectangle rect = new Rectangle(x,y,1,1);
        for(Button button : buttons){
            if(Intersector.overlaps(rect,button.getSprite().getBoundingRectangle()) && button.getId() != -1){
                returnValue = button.getId();
                break;
            }
        }
        return returnValue;
    }

    @Override
    public void dispose() {
        for(Button button : buttons){
            button.dispose();
        }
    }
}
