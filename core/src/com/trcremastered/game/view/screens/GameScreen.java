package com.trcremastered.game.view.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.trcremastered.game.view.Button;

public class GameScreen extends Screen{

    public GameScreen(){
        super(768,432);
        id = 1;
    }

    @Override
    public void timer() {

    }

    @Override
    public void render(SpriteBatch batch) {

    }

    @Override
    public int touchDown(int x, int y) {
        int returnValue = -1;
        Rectangle rect = new Rectangle(x,y,1,1);
        for(Button button : buttons){
            if(Intersector.overlaps(rect,button.getSprite().getBoundingRectangle())){
                returnValue = button.getId();
                System.out.println(returnValue);
                break;
            }
        }
        return returnValue;
    }

    @Override
    public void dispose() {

    }
}
