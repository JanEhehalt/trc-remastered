package com.trcremastered.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Button {
    int id;
    Sprite sprite;

    public Button(int id, int xPos, int yPos, String texture){
        this.id = id;
        this.sprite = new Sprite(new Texture(Gdx.files.internal(texture)));
        sprite.setX(xPos);
        sprite.setY(yPos);
    }

    public void dispose(){
        sprite.getTexture().dispose();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }


}
